/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017
Aktualisiert: 26. Juni 2017

MaschinenFabrik Klasse für Rainiers Tutorien, SoSe 2017
Zum Testen der Klasse Maschine, deren Unterklasse Reiniger, und deren Unterklasse SmallReiniger
*/

public class MaschinenFabrik {
	
	public static void main(String[] args) {

		Maschine.showAnzahl();
		System.out.println("---");
		
		Maschine machine = new Maschine("meine Maschine", 0.5);
		Reiniger cleaner = new Reiniger("Der Reiniger");
		Reiniger anotherCleaner = new Reiniger("Bob", 42);
		SmallReiniger smallCleaner = new SmallReiniger("Der kleine Reiniger");
		SmallReiniger blueCleaner = new SmallReiniger("Der blaue Reiniger", "blau");

		machine.showInfo();
		System.out.println("---");
		cleaner.showInfo();
		System.out.println("---");
		smallCleaner.showInfo();
		System.out.println("---");
		
		Maschine.showAnzahl();
		System.out.println("---");

		Maschine testmach = new Maschine();
		testmach.showInfo();
		System.out.println("---");

		/*
		Exception in thread "main" java.lang.ClassCastException: Maschine cannot be cast to Reiniger
	at MaschinenFabrik.main(MaschinenFabrik.java:34)
		Reiniger testrein = (Reiniger) testmach;
		testmach.showInfo();
		testrein.showInfo();
		*/

		/* explizite Typumwandlung */
		Reiniger reinig = new Reiniger();
		Maschine masch = (Reiniger) reinig;
		reinig.showInfo();
		System.out.println("---");
		masch.showInfo();
		System.out.println("---");
		reinig.reinigen(machine);
		System.out.println("---");
//		masch.reinigen(machine);

		/* implizite Typumwandlung */
		Reiniger testrein2 = new Reiniger();
		Maschine testmach2 = testrein2;
		testrein2.showInfo();
		System.out.println("---");
		testmach2.showInfo();
		System.out.println("---");

		Maschine.showAnzahl();
		System.out.println("---");
		Reiniger.showAnzahl(); // es gibt keine Klassenattribute Anzahl für Reiniger und keine (überschriebene) Methode getAnzahl() in Reiniger
		System.out.println("---");
		SmallReiniger.showAnzahl(); // es gibt keine Klassenattribute Anzahl für SmallReiniger und keine (überschriebene) Methode getAnzahl() in SmallReiniger
		System.out.println("---");

		/*
		In der (Unter)Klasse Reiniger habe ich getAnzahl() und showAnzahl() implementiert, und die entsprechende Zählervariable initialisiert und in den Konstruktoren eingestellt.
		Ihr könnt dann testen, wie showAnzahl() angewendet auf Reiniger oder SmallReiniger mit dieser Methode aussieht.
		*/
		
	}
}