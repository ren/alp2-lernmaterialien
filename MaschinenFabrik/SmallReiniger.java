/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

SmallReiniger Klasse für Rainiers Tutorien, SoSe 2017
Beispiel für Unterklassen einer Unterklasse
*/

public class SmallReiniger extends Reiniger {
	
	public SmallReiniger() {}

	public SmallReiniger(String name) {
		super(name,1.25);
	}

	public SmallReiniger(String name, String color) {
		super(name,1.25);
		this.setFarbe(color);
	}

}