/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Maschine Klasse für Rainiers Tutorien, SoSe 2017
*/

public class Maschine {
	private double laenge = 1.0;
	private double breite = 1.0;
	private double hoehe = 1.0;
	private double gewicht = 1.0;
	private int alter = 0;
	private String name;
	private String farbe = "grau";
	private boolean istAn;
	private boolean istKaputt;
	private boolean istSchmutzig;
	private static int anzahl;

	public Maschine() {
		anzahl++;
	}

	public Maschine(String name) {
		this.name = name;
		anzahl++;
	}

	public Maschine(String name, double size) {
		this.name = name;
		hoehe = size;
		breite = size;
		laenge = size;
		gewicht = this.getVolume() * size;
		anzahl++;
	}

	public void setLaenge(double x) {
		laenge = x;
	}

	public double getLaenge() {
		return laenge;
	}

	public void setBreite(double x) {
		breite = x;
	}

	public double getBreite() {
		return breite;
	}

	public void setHoehe(double x) {
		hoehe = x;
	}

	public double getHoehe() {
		return hoehe;
	}

	public void setGewicht(double x) {
		gewicht = x;
	}

	public double getGewicht() {
		return gewicht;
	}

	public double getVolume() {
		return hoehe*breite*laenge;
	}

	public void setAlter(int x) {
		alter = x;
	}

	public int getAlter() {
		return alter;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getFarbe() {
		return farbe;
	}

	public boolean istAn() {
		return istAn;
	}

	public void anschalten() {
		istAn = true;
	}

	public void ausschalten() {
		istAn = false;
	}

	public boolean istKaputt() {
		return istKaputt;
	}

	public void setKaputt(boolean b) {
		istKaputt = b;
	}

	public boolean istSchmutzig() {
		return istSchmutzig;
	}

	public void setSchmutzig(boolean b) {
		istSchmutzig = b;
	}

	public static int getAnzahl() {
		return anzahl;
	}

	public static void showAnzahl() {
		System.out.println("Es gibt "+getAnzahl()+" Maschinen.");
	}

	public void showInfo() {
		System.out.println("Alle Infos über "+getName()+":");
		System.out.println("Länge: "+getLaenge());
		System.out.println("Breite: "+getBreite());
		System.out.println("Höhe: "+getHoehe());
		System.out.println("Gewicht: "+getGewicht());
		System.out.println("Volume: "+getVolume());
		System.out.println("Alter: "+getAlter());
		System.out.println("Farbe: "+getFarbe());
		System.out.println("Ist an: "+istAn());
		System.out.println("Ist kaputt: "+istKaputt());
		System.out.println("Ist schmutzig: "+istSchmutzig());
	}

}