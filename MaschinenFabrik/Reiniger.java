/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017
Aktualisiert: 26. Juni 2017

Reiniger Klasse für Rainiers Tutorien, SoSe 2017
Beispiel für Unterklassen
*/

public class Reiniger extends Maschine {
//	private static int anzahl;

	public Reiniger() {
//		anzahl++;
	}

	public Reiniger(String name) {
		super(name,2.5);
		this.setFarbe("weiß");
//		anzahl++;
	}

	public Reiniger(String name, double size) {
		super(name,size);
		this.setFarbe("weiß");
//		anzahl++;
	}

	public void reinigen(Maschine m) {
		if (m.istSchmutzig()) {
			m.setSchmutzig(false);
			System.out.println(m.getName()+" ist jetzt sauber!");
		} else {
			System.out.println(m.getName()+" ist aber doch sauber.");
		}
	}

	public void showInfo() {
		System.out.println("Infos über "+getName()+":");
		System.out.println("Maschinen Typ: Reiniger");
		System.out.println("Farbe: "+getFarbe());
	}

	/*
	public static int getAnzahl() {
		return anzahl;
	}

	public static void showAnzahl() {
		System.out.println("Es gibt "+getAnzahl()+" Reiniger.");
	}
	*/
	
}