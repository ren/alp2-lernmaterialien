/*
Autor: Rainier Raymond Robles
Datum: 16. Juni 2017

Maschine Klasse für Rainiers Tutorien, SoSe 2017
*/

public class Maschine {
	private double laenge = 1.0;
	private double breite = 1.0;
	private double hoehe = 1.0;
	private double gewicht = 1.0;
	private String name;
	private String farbe = "grau";
	private boolean istAn;
	private boolean istKaputt;
	private static int anzahl;

	public Maschine() { // default Konstruktor
		anzahl++;
	}

	public Maschine(String name) { // Konstruktor mit deklariertem Name
		this.name = name;
		anzahl++;
	}

	public Maschine(String name, double size) { // Konstruktor mit Name und benutzerdefinierte Parameter
		this.name = name;
		hoehe = size;
		breite = size;
		laenge = size;
		gewicht = this.getVolume() * size;
		anzahl++;
	}

	public double getLaenge() {
		return laenge;
	}

	public double getBreite() {
		return breite;
	}

	public double getHoehe() {
		return hoehe;
	}

	public double getGewicht() {
		return gewicht;
	}

	public double getVolume() {
		return hoehe*breite*laenge;
	}

	public String getName() {
		return name;
	}

	public void anschalten() {
		istAn = true;
	}

	public void ausschalten() {
		istAn = false;
	}

	public void reparieren() { // Maschine repariert sich selbst
		istKaputt = false;
	}

	public void reparieren(Maschine machine) { // Maschine repariert eine andere Maschine
		machine.istKaputt = false;
	}

	public static int getAnzahl() {
		return anzahl;
	}

	public static void main(String[] args) {
		/* erzeugt vier Instanzen mit drei Konstruktoren */
		Maschine ersteMaschine = new Maschine("Meine erste Maschine");
		Maschine zweiteMaschine = new Maschine("Meine zweite Maschine");
		Maschine dritteMaschine = new Maschine();
		Maschine customMaschine = new Maschine("custom Maschine", 5.0);
		
		/* Test mit ersteMaschine... wie sehen die andere Maschinen aus? */
		System.out.println("Alles über "+ersteMaschine.getName()+":");
		System.out.println("Hoehe: "+ersteMaschine.getHoehe());
		System.out.println("Breite: "+ersteMaschine.getBreite());
		System.out.println("Laenge: "+ersteMaschine.getLaenge());
		System.out.println("Gewicht: "+ersteMaschine.getGewicht());
		double ersteMaschineVolume = ersteMaschine.getVolume();
		System.out.println("Volume: "+ersteMaschineVolume);

		/* testet unsere statische Methode */
		System.out.println("Es gibt "+getAnzahl()+" Maschinen.");

		/* testet die überladene reparieren() Methode */
		customMaschine.istKaputt = true;
		dritteMaschine.istKaputt = true;
		System.out.println("Ist "+customMaschine.getName()+" kaputt? "+customMaschine.istKaputt);
		System.out.println("Ist "+dritteMaschine.getName()+" kaputt? "+dritteMaschine.istKaputt);
		customMaschine.reparieren(); // Methode ohne Parameter
		customMaschine.reparieren(dritteMaschine); // Methode mit Parameter
		System.out.println("Ist "+customMaschine.getName()+" kaputt? "+customMaschine.istKaputt);
		System.out.println("Ist "+dritteMaschine.getName()+" kaputt? "+dritteMaschine.istKaputt);

	}
}