public class MaschineAnders {
	
	public static void main(String[] args) {
		/* erzeugt vier Instanzen mit drei Konstruktoren */
		Maschine ersteMaschine = new Maschine("Meine erste Maschine");
		Maschine zweiteMaschine = new Maschine("Meine zweite Maschine");
		Maschine dritteMaschine = new Maschine();
		Maschine customMaschine = new Maschine("custom Maschine", 5.0);
		
		/* Test mit ersteMaschine... wie sehen die andere Maschinen aus? */
		System.out.println("Alles über "+ersteMaschine.getName()+":");
		System.out.println("Hoehe: "+ersteMaschine.getHoehe());
		System.out.println("Breite: "+ersteMaschine.getBreite());
		System.out.println("Laenge: "+ersteMaschine.getLaenge());
		System.out.println("Gewicht: "+ersteMaschine.getGewicht());
		double ersteMaschineVolume = ersteMaschine.getVolume();
		System.out.println("Volume: "+ersteMaschineVolume);

		/* Folgende Befehle funktionieren einfach nicht. Wieso? Funktionieren die in der anderen Klasse?
		System.out.println("Hoehe: "+ersteMaschine.hoehe);
		System.out.println("Es gibt "+Maschine.anzahl+" Maschinen.");
		*/
	}
}