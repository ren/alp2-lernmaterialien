## KVV Simulator
Autor: Rainier Raymond Robles

Datum: 03. Juli 2017

Ziel: Erzeugung, Benutzung, und Behandlung von selbstdefinierte Ausnahmen.

(Ja ja, ich weiss, `TutorIn` und `Tutorium` sind zu ähnlich als Klassennamen....)

#### OverworkedException.java und TutoriumFullException.java
Unsere zwei selbstdefinierte Ausnahmen.

#### Studi.java
Klasse, die Studierende als Objekte/Instanzen erzeugt.

#### TutorIn.java
Klasse, die Tutor_innen als Objekte/Instanzen erzeugt. Unterklasse von Studi, da Tutor_innen (in der Regel) auch Studierende sind

#### Tutorium.java
Klasse, die Tutorien als Objekte/Instanzen erzeugz.

#### KVV.java
Wo unsere main Methode wohnt. Verwaltet unsere Tutorien (oder zumindest simuliert es).