/*
Autor: Rainier Raymond Robles
Datum: 03. Juli 2017

TutorIn Klasse für Rainiers Tutorien, SoSe 2017
Erzeugt Tutor_innen für unsere KVV Simulation
Beispiel für Ausnahmebehandlung in Java
*/

import java.util.*;

public class TutorIn extends Studi { // Tutor_innen sind auch Studierende!
	private int max = 2; // alle Tutor_innen dürfen normalerweise nur 2 Tutorien haben
	private ArrayList<Tutorium> allsections = new ArrayList<Tutorium>(); // Liste aller Tutorien eines Tutors/einer Tutorin
	private static ArrayList<TutorIn> alltuts = new ArrayList<TutorIn>(); // Liste aller Tutor_innen

	/* Konstruktoren */

	public TutorIn(String name, int semester) {
		super(name, semester);
		alltuts.add(this);		
	}

	public TutorIn(String name, int semester, int max) { // falls Tutor_in mehr (oder weniger) als zwei Tutorien haben darf
		super(name, semester);
		this.max = max;
		alltuts.add(this);		
	}

	/* verschiedene get- und set-Methode */
	/* natürlich sind einige Methode aus der Studi Klasse geerbt */

	public int getMax() {
		return max;
	}

	public static ArrayList<TutorIn> getTutList() {
		return alltuts;
	}

	/* gibt die Anzahl der Tutorien, die Tutor_in schon hat, zurück */

	public int getAssigned() {
		return allsections.size();
	}

	/* fügt Tutorium in allsections hinzu */

	public void addSection(Tutorium t) {
		allsections.add(t);
	}

	/* iteriert durch die Liste der Tutor_innen, gibt index und Name zurück */
	/* überschreibt Methode der Studi Klasse */
	/* wenn TutorIn.showAllStudis() ausgeführt wird, wird die Liste aller Tutor_innen und nicht alle Studierenge zurückgegeben */

	public static void showAllStudis() {
		for (TutorIn studi : alltuts) {
			System.out.println(alltuts.indexOf(studi)+" "+studi.getName());
		}
	}
}