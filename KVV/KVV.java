/*
Autor: Rainier Raymond Robles
Datum: 03. Juli 2017

KVV Klasse für Rainiers Tutorien, SoSe 2017
Hier wohnt die main Methode für unsere KVV Simulation
Beispiel für Ausnahmebehandlung in Java
*/

public class KVV {
	public static void main(String[] args) {

		/* erzeugt 10 (generische) Studierende */
		Studi[] students = new Studi[10];
		String studname = "Studi ";
		for (int i = 0; i < 10; i++) {
			String fullname = studname + Integer.toString(i);
			students[i] = new Studi(fullname, 2);
		}

		/* erzeugt zwei Tutor_innen; merke: Anja darf 4 Tutorien haben, Rainier nur 2 (default) */
		TutorIn rainier = new TutorIn("Rainier",4);
		TutorIn anja = new TutorIn("Anja",12,4);

		/* erzeugt fünf Tutorien */
		Tutorium tut01 = new Tutorium("ALP2 Tut01","Mo",8,10);
		Tutorium tut02 = new Tutorium("ALP2 Tut02","Mo",10,12);
		Tutorium tut03 = new Tutorium("ALP2 Tut03","Di",14,16);
		Tutorium tut04 = new Tutorium("ALP2 Tut04","Mi",14,16);
		Tutorium tut05 = new Tutorium("ALP2 Tut05","Mi",16,18);

		/* fügt Tutor_innen in Tutorien hinzu */
		tut01.setTutorin();
		System.out.println("-----");
		tut02.setTutorin();
		System.out.println("-----");
		tut03.setTutorin();
		System.out.println("-----");
		tut04.setTutorin();
		System.out.println("-----");
		tut05.setTutorin();
		System.out.println("-----");

		/* zeigt alle Tutorien */
		Tutorium.showTutorials();
		System.out.println("-----");

		/* fügt Studierende in Tutorien hinzu */
		for (Studi student : students) {
			student.joinTutorial();
			System.out.println("-----");
		}
		
	}
}