/*
Autor: Rainier Raymond Robles
Datum: 03. Juli 2017

Tutorium Klasse für Rainiers Tutorien, SoSe 2017
Erzeugt Tutorien für unsere KVV Simulation
Beispiel für Ausnahmebehandlung in Java
*/

import java.util.*;

public class Tutorium {
	private String name;
	private String weekday;
	private int starttime;
	private int endtime;
	private int capacity = 2; // Kapazität eines Tutoriums; ist klein, damit wir die Exceptions testen können!
	private TutorIn tutor_in;
	private ArrayList<Studi> studentlist = new ArrayList<Studi>(); // Liste aller Studierende, die im Tutorium angemeldet sind
	private static ArrayList<Tutorium> tutlist = new ArrayList<Tutorium>(); // Liste aller Tutorien

	/* Konstruktor */

	public Tutorium(String name, String weekday, int starttime, int endtime) {
		this.name = name;
		this.weekday = weekday;
		this.starttime = starttime;
		this.endtime = endtime;
		tutlist.add(this);
	}

	/* verschiedene get- und set-Methode */

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}

	public String getWeekday() {
		return weekday;
	}

	public void setStarttime(int starttime) {
		this.starttime = starttime;
	}

	public int getStarttime() {
		return starttime;
	}

	public void setEndtime(int endtime) {
		this.endtime = endtime;
	}

	public int getEndtime() {
		return endtime;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getCapacity() {
		return capacity;
	}

	public TutorIn getTutorin() {
		return tutor_in;
	}

	public static ArrayList<Tutorium> getTutList() {
		return tutlist;
	}

	/* gibt Anzahl der Studierende zurück, die fürs ausgewählte Tutorium angemeldet sind */

	public int getSize() {
		return studentlist.size();
	}

	/* versucht, ein_e ausgewählte Tutor_in t als Tutor eines Tutorium hinzuzufügen */
	/* überladene Methode und wird geschrieben nur damit wir OverworkedException werfen können */

	private void setTutorin(TutorIn t) throws OverworkedException {
		if (t.getMax() == t.getAssigned()) {
			throw new OverworkedException(); // Ausnahme wird geworfen falls Tutor_in schon genau capacity viel Tutorien hat
		} else {
			this.tutor_in = t; // sonst funktioniert es einwandfrei
			t.addSection(this); // Methode der Klasse TutorIn
		}		
	}

	/* überladene Methode; diese Variante wird in KVV benutzt und fragt nach Benutzereingabe */

	public void setTutorin() {
		ArrayList<TutorIn> tuts = TutorIn.getTutList(); // nimmt Liste von Tutor_innen aus der TutorIn Klasse
		Scanner sc = new Scanner(System.in); // Befehl um Eingabe aus der Kommandozeile zu ermöglichen
		System.out.println(this.tutInfo());
		System.out.println("Wer soll Tutor_in dieses Tutoriums sein?");
		TutorIn.showAllStudis(); // zeigt Liste von alle Tutor_innen; Methode der Klasse TutorIn
		System.out.println("Wähle eine Zahl:");
		boolean validInput = false; // initialisiert Abbruchsbedingung
		while (!validInput) {
			try { // Methode, die Ausnahmen auslösen können
				int i = sc.nextInt(); // castet Benutzereingabe als int --> kann Ausnahme ausführen
				this.setTutorin(tuts.get(i)); // Tutor_in wird in ausgewähltes Tutorium hinzugefügt --> kann Ausnahme ausführen
				System.out.println("TutorIn hinzugefügt!");
				System.out.println(this.tutInfo());
				validInput = true; // Schleife wird beendet
			} catch (IndexOutOfBoundsException io) { // falls int kein Element der Listen von Tutor_innen entspricht
				System.out.println("Diese Person existiert nicht.");
				System.out.println("Wähle eine Zahl:"); // danach weiter zum Anfang der Schleife
			} catch (OverworkedException ow){ // falls ausgewähltes Tutor_in genau capacity viel Tutorien hat
				System.out.println(ow);
				System.out.println("Wähle eine Zahl:"); // danach weiter zum Anfang der Schleife
			} catch (InputMismatchException ee) { // falls Benutzereingabe nicht ins int castbar
				System.out.println("Keine gültige Eingabe.");
				System.out.println("Wähle eine Zahl:");
				sc.next(); // "löscht"/nutzt Benutzereingabe, macht Scanner bereit für neue Eingabe; danach weiter zum Anfang der Schleife
			}
		}
	}

	/* fügt Studierende studi in ausgewähltes Tutorium hinzu; wirft TutoriumFullException, siehe Klasse Studi für die genaue Ausnahmebehandlung */

	public void addStudent(Studi studi) throws TutoriumFullException {
		if (this.studentlist.size() == this.capacity) {
		 	throw new TutoriumFullException();
		} else {
			this.studentlist.add(studi);
		}		
	}

	/* gibt String mit alle Infos zum Tutorium zurück */

	public String tutInfo() {
		try {
			return "Tutorium: "+name+" | "+weekday+" "+Integer.toString(starttime)+"-"+Integer.toString(endtime)+" | TutorIn: "+tutor_in.getName()+" | Kapazität: "+capacity+" | #Studierende: "+studentlist.size();
		} catch (Exception e) {
			return "Tutorium: "+name+" | "+weekday+" "+Integer.toString(starttime)+"-"+Integer.toString(endtime)+" | TutorIn: bis jetzt keine | Kapazität: "+capacity+" | #Studierende: "+studentlist.size();
		}
		
	}

	/* iteriert durch die Liste der Tutorien, gibt index und Name zurück */

	public static void showTutorials() {
		for (Tutorium tutorial : tutlist) {
			System.out.println(tutlist.indexOf(tutorial)+" "+tutorial.tutInfo());
		}
	}

	/* iteriert durch die Liste der Studierende, die im ausgewählten Tutorium angemeldet sind, und gibt die Namen zurück */

	public void showStudis() {
		System.out.println("Studis in "+this.tutInfo());
		for (Studi studi : this.studentlist) {
			System.out.println(studi.getName());
		}
	}
}