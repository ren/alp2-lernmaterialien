/*
Autor: Rainier Raymond Robles
Datum: 03. Juli 2017

Studi Klasse für Rainiers Tutorien, SoSe 2017
Erzeugt Studierende für unsere KVV Simulation
Beispiel für Ausnahmebehandlung in Java
*/

import java.util.*;

public class Studi {
	private String name;
	private int matrikelnummer;
	private int semester;
	private boolean immatrikuliert = true; // wird eigentlich hier nicht benutzt
	private static int counter = 0; // zählt Anzahl der Studierende, wird benutzt um Matrikelnummer zu erzeugen
	private static ArrayList<Studi> allstudis = new ArrayList<Studi>(); // Liste aller Studierende

	/* Konstruktor */

	public Studi(String name, int semester) {
		this.name = name;
		this.matrikelnummer = 100000+counter;
		this.semester = semester;
		allstudis.add(this);
		counter++;
	}

	/* verschiedene get- und set-Methode */

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setMnummer(int matrikelnummer) {
		this.matrikelnummer = matrikelnummer;
	}

	public int getMnummer() {
		return matrikelnummer;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public int getSemester() {
		return semester;
	}

	public void setImmatrikuliert(boolean immatrikuliert) {
		this.immatrikuliert = immatrikuliert;
	}

	public boolean getImmatrikuliert() {
		return immatrikuliert;
	}

	public static ArrayList<Studi> getStudList() {
		return allstudis;
	}

	/* druckt Info über ein_e Student_in */

	public void getInfo() {
		System.out.println("Name: "+this.getName());
		System.out.println("Matrikelnummer: "+this.getMnummer());
		System.out.println("Semester: "+this.getSemester());
		System.out.println("-----");
	}

	/* iteriert durch die Liste der Studierende, gibt index und Name zurück */

	public static void showAllStudis() {
		for (Studi studi : allstudis) {
			System.out.println(allstudis.indexOf(studi)+" "+studi.getName());
		}
	}

	/* für die Anmeldung einer Studierende in einem Tutorium */

	public void joinTutorial() {
		ArrayList<Tutorium> tuts = Tutorium.getTutList(); // nimmt Liste von Tutorien aus der Tutorium Klasse
		Scanner sc = new Scanner(System.in); // Befehl um Eingabe aus der Kommandozeile zu ermöglichen
		System.out.println("Für welches Tutorium soll "+this.getName()+" sich anmelden?");
		Tutorium.showTutorials(); // zeigt Liste von alle Tutorien; Methode der Klasse Tutorium
		System.out.println("Wähle eine Zahl:");
		boolean validInput = false; // initialisiert Abbruchsbedingung
		while (!validInput) {
			try { // Methode, die Ausnahmen auslösen können
				int i = sc.nextInt(); // castet Benutzereingabe als int --> kann Ausnahme ausführen
				tuts.get(i).addStudent(this); // Studierende wird in ausgewähltes Tutorium hinzugefügt; Methode der Klasse Tutorium --> kann Ausnahme ausführen
				System.out.println("Angemeldet!");
				System.out.println(tuts.get(i).tutInfo()); // Methode der Klasse Tutorium
				validInput = true; // Schleife wird beendet
			} catch (IndexOutOfBoundsException io) { // falls int kein Element der Listen von Tutorien entspricht
				System.out.println("Dieses Tutorium existiert nicht.");
				System.out.println("Wähle eine Zahl:"); // danach weiter zum Anfang der Schleife
			} catch (TutoriumFullException ow){ // falls ausgewähltes Tutorium voll ist
				System.out.println(ow);
				System.out.println("Wähle eine Zahl:"); // danach weiter zum Anfang der Schleife
			} catch (InputMismatchException ee) { // falls Benutzereingabe nicht ins int castbar
				System.out.println("Keine gültige Eingabe.");
				System.out.println("Wähle eine Zahl:");
				sc.next(); // "löscht"/nutzt Benutzereingabe, macht Scanner bereit für neue Eingabe; danach weiter zum Anfang der Schleife
			}
		}
	}
}