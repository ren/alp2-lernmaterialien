/*
Autor: Rainier Raymond Robles
Datum: 03. Juli 2017

TutoriumFullException Ausnahme für Rainiers Tutorien, SoSe 2017
Beispiel für Ausnahme
*/

class TutoriumFullException extends Exception {
	public TutoriumFullException() {
		super("Dieses Tutorium ist leider voll!");
	}
}