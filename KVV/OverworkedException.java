/*
Autor: Rainier Raymond Robles
Datum: 03. Juli 2017

OverworkedException Ausnahme für Rainiers Tutorien, SoSe 2017
Beispiel für Ausnahme
*/

class OverworkedException extends Exception {
	public OverworkedException() {
		super("Das ist zu viel! Diese Person kann keine Tutorien mehr leiten.");
	}
}