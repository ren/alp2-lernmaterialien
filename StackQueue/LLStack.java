/*
Autor: Rainier Raymond Robles
Datum: 11. Juli 2017

LLStack Klasse für Rainiers Tutorien, SoSe 2017
Beispiel Implementierung eines Stapels mit Hilfe einer einfach verketteten Liste in Java
*/

public class LLStack<T> {
	private LLNode<T> head;
	private int size;

	public LLStack() {
		head = null;
		size = 0;
	}

	public void push(T elem) {
		LLNode<T> newNode = new LLNode<T>(elem);
		newNode.setNext(head);
		head = newNode;
		size++;
	}

	public T pop() throws StackEmptyException {
		if (!isEmpty()) {
			T elem = head.getElem();
			head = head.getNext();
			size--;
			return elem;
		} else {
			throw new StackEmptyException();
		}
	}

	public T top() throws StackEmptyException {
		if (!isEmpty()) {
			return head.getElem();
		} else {
			throw new StackEmptyException();
		}
	}

	public boolean isEmpty() {
		return head == null;
	}

	public int size() {
		return size;
	}

	public static void main(String[] args) {
		LLStack<Integer> intstack = new LLStack<Integer>();

		System.out.println(intstack.isEmpty());
		System.out.println(intstack.size());

		for (int i = 1; i < 6; i++) {
			intstack.push(i);
			System.out.println(intstack.isEmpty());
			System.out.println(intstack.size());
		}

		for (int i = 1; i <= 6; i++) {
			try {
				System.out.println(intstack.top());
				System.out.println(intstack.isEmpty());
				System.out.println(intstack.size());
				System.out.println(intstack.pop());
				System.out.println(intstack.isEmpty());
				System.out.println(intstack.size());
			} catch (StackEmptyException see) {
				System.out.println(see);
			}
		}

	}
}