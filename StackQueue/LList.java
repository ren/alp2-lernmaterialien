/*
Autor: Rainier Raymond Robles
Datum: 11. Juli 2017

LList Klasse für Rainiers Tutorien, SoSe 2017
Beispiel Implementierung einer einfach verketteten Liste in Java
*/

public class LList<T> {
	private LLNode<T> head = new LLNode<T>(); // Implementierung mit Dummy-Element als Zeiger auf Kopf der Liste
	private int size;

	public LList() {
		size = 0;
	}

	public void insertElem(T elem) { // am Ende der Liste hinzufügen
		LLNode<T> newNode = new LLNode<T>(elem);
		LLNode<T> current = head;
		while (current.getNext() != null) {
			current = current.getNext();
		}
		current.setNext(newNode);
		size++;
	}

	public void insertElem(T elem, int index) throws IndexOutOfBoundsException { // an einer bestimmten Stelle hinzufügen
		LLNode<T> newNode = new LLNode<T>(elem);
		LLNode<T> prev = head;
		LLNode<T> current = prev.getNext();
		int position = 0;
		if ((index >= size) || (index < 0)) {
			throw new IndexOutOfBoundsException();
		} else {
			while (position < index) {
				prev = current;
				current = prev.getNext();
				position++;
			}
			newNode.setNext(current);
			prev.setNext(newNode);
			size++;
		}
	}

	public void delElem(int index) throws IndexOutOfBoundsException { // Element an einer bestimmten Stelle löschen
		LLNode<T> prev = head;
		LLNode<T> current = head.getNext();
		int position = 0;
		if ((index >= size) || (index < 0)) {
			throw new IndexOutOfBoundsException();
		} else {
			while (position < index) {
				prev = current;
				current = prev.getNext();
				position++;
			}
			prev.setNext(current.getNext());
			current.setNext(null);
			size--;
		}
	}

	public T getElem(int index) throws IndexOutOfBoundsException { // Element an einer bestimmten Stelle zurückgeben
		if ((index >= size) || (index < 0)) {
			throw new IndexOutOfBoundsException();
		} else {
			LLNode<T> current = head.getNext();
			int position = 0;
			while (position < index) {
				current = current.getNext();
				position++;
			}
			return current.getElem();
		}
	}

	public void showList() {
		LLNode<T> current = head.getNext();
		while (current != null) {
			System.out.println(current.getElem());
			current = current.getNext();
		}
	}

	public static void main(String[] args) {
		LList<Integer> intll = new LList<Integer>();
		intll.showList();
		intll.insertElem(0);
		intll.insertElem(1);
		intll.insertElem(3);
		intll.insertElem(4);
		System.out.println("Initial List:");
		intll.showList();

		try {
			System.out.println("Result 1:");
			intll.insertElem(5,4);
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 2:");
			intll.insertElem(2,2);
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 3:");
			intll.insertElem(0,0);
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}

		try {
			System.out.println("Result 4:");
			intll.delElem(6);
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 5:");
			intll.delElem(5);
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 6:");
			intll.delElem(0);
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 7:");
			intll.delElem(2);
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}

		try {
			System.out.println("Result 8:");
			System.out.println(intll.getElem(3));
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 9:");
			System.out.println(intll.getElem(2));
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 10:");
			System.out.println(intll.getElem(0));
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}
		try {
			System.out.println("Result 11:");
			System.out.println(intll.getElem(1));
			intll.showList();
		} catch (IndexOutOfBoundsException ioe) {
			System.out.println(ioe);
		}

	}
}