/*
Autor: Rainier Raymond Robles
Datum: 11. Juli 2017

LLNode Klasse für Rainiers Tutorien, SoSe 2017
Beispiel Implementierung eines Knotens für einfach verkettete Listen in Java
*/

public class LLNode<T> {
	private LLNode<T> next;
	private T elem;

	public LLNode(T elem, LLNode<T> next) {
		this.elem = elem;
		this.next = next;
	}

	public LLNode(T elem) {
		this(elem, null);
	}

	public LLNode() {
		this(null, null);
	}

	public T getElem() {
		return elem;
	}

	public void setElem(T elem) {
		this.elem = elem;
	}

	public LLNode<T> getNext() {
		return next;
	}

	public void setNext(LLNode<T> next) {
		this.next = next;
	}
}