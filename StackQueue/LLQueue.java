public class LLQueue<T> {
	private LLNode<T> head;
	private LLNode<T> tail;
	private int size;

	public LLQueue() {
		head = null;
		tail = null;
		size = 0;
	}
	
	public void enqueue(T elem) {
		LLNode<T> newNode = new LLNode<T>(elem);
		if (isEmpty()) {
			head = newNode;
		} else {
			tail.setNext(newNode);
		}
		tail = newNode;
		size++;
	}

	public T dequeue() throws QueueEmptyException {
		if (!isEmpty()) {
			T elem = head.getElem();
			head = head.getNext();
			size--;
			return elem;
		} else {
			throw new QueueEmptyException();
		}
	}

	public T head() throws QueueEmptyException {
		if (!isEmpty()) {
			return head.getElem();
		} else {
			throw new QueueEmptyException();
		}
	}

	public boolean isEmpty() {
		return head == null;
	}

	public int size() {
		return size;
	}

	public static void main(String[] args) {
		LLQueue<Integer> intqueue = new LLQueue<Integer>();

		System.out.println(intqueue.isEmpty());
		System.out.println(intqueue.size());

		for (int i = 1; i < 6; i++) {
			intqueue.enqueue(i);
			System.out.println(intqueue.isEmpty());
			System.out.println(intqueue.size());
		}

		for (int i = 1; i <= 6; i++) {
			try {
				System.out.println(intqueue.head());
				System.out.println(intqueue.isEmpty());
				System.out.println(intqueue.size());
				System.out.println(intqueue.dequeue());
				System.out.println(intqueue.isEmpty());
				System.out.println(intqueue.size());
			} catch (QueueEmptyException qee) {
				System.out.println(qee);
			}
		}

	}
}