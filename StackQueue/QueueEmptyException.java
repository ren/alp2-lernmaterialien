public class QueueEmptyException extends Exception {
	public QueueEmptyException() {
		super("Warteschlange ist leer!");
	}
}