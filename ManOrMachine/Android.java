/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Android Klasse für Rainiers Tutorien, SoSe 2017
Beispiel für die Implementierung einer Schnittstelle
*/

public class Android implements Mensch, Roboter {
	private String name;

	public Android(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void gruessen() {
		System.out.println("BEEEEEEEP HALLO ICH BIN "+getName());
	}

	public void essen() {
		System.out.println(getName()+" ISST SCHRAUBEN");
	}

	public void trinken() {
		System.out.println(getName()+" TRINKT BENZIN");
	}

	public void schlafen(int i) {
		System.out.println(getName()+"  NICHT VERFÜGBAR FÜR "+i+" MINUTEN");
	}

	public void bewegen(int i) {
		System.out.println(getName()+" BEWEGT SICH "+i+"KM NACH VORNE");
	}

	public void tanzen() {
		System.out.println(getName()+" DREHT SICH ZWEIMAL UM");
	}

	public void reparieren() {
		System.out.println("I'LL BE BACK");
	}

	public void aufladen() {
		System.out.println("BATTERIE JETZT 100%");
	}
	
	public void aktualisieren() {
		System.out.println("ICH BIN WIE NEU GEBOREN");
	}

}