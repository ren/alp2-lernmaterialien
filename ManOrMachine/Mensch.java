/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Mensch Schnittstelle für Rainiers Tutorien, SoSe 2017
Beispiel für die Implementierung einer Schnittstelle
*/

public interface Mensch {
	void gruessen();
	void essen();
	void trinken();
	void schlafen(int i);
	void bewegen(int i);
	void tanzen();
}