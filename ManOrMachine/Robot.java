/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Robot Klasse für Rainiers Tutorien, SoSe 2017
Beispiel für die Implementierung einer Schnittstelle
*/

public class Robot implements Roboter {
	private String name;

	public Robot(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void gruessen() {
		System.out.println("BEEP BEEP BEEP");
	}

	public void bewegen(int i) {
		System.out.println("SYSTEM MESSAGE: "+getName()+" bewegt sich "+i+"km nach vorne.");
	}

	public void reparieren() {
		System.out.println("SYSTEM MESSAGE: "+getName()+" wird repariert.");
	}

	public void aufladen() {
		System.out.println("SYSTEM MESSAGE: Batterie jetzt 100%.");
	}
	
	public void aktualisieren() {
		System.out.println("SYSTEM MESSAGE: Software aktualisiert.");
	}

}