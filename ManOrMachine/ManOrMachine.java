/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

ManOrMachine Klasse für Rainiers Tutorien, SoSe 2017
Beispiel für die Implementierung einer Schnittstelle
*/

public class ManOrMachine {
	
	public static void main(String[] args) {

		Human mensch = new Human("Bob");
		Robot roboter = new Robot("BB-8");
		Android trmn8r = new Android("T-800");

		mensch.gruessen();
		mensch.essen();
		mensch.trinken();
		mensch.schlafen(1);
		mensch.bewegen(30);
		mensch.tanzen();

		System.out.println("---");

		roboter.gruessen();
		roboter.bewegen(30);
		roboter.reparieren();
		roboter.aufladen();
		roboter.aktualisieren();

		System.out.println("---");

		trmn8r.gruessen();
		trmn8r.essen();
		trmn8r.trinken();
		trmn8r.schlafen(30);
		trmn8r.bewegen(30);
		trmn8r.tanzen();
		trmn8r.reparieren();
		trmn8r.aufladen();
		trmn8r.aktualisieren();

	}
}