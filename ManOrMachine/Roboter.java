/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Roboter Schnittstelle für Rainiers Tutorien, SoSe 2017
Beispiel für die Implementierung einer Schnittstelle
*/

public interface Roboter {
	void gruessen();
	void bewegen(int i);
	void reparieren();
	void aufladen();
	void aktualisieren();
}