/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Human Klasse für Rainiers Tutorien, SoSe 2017
Beispiel für die Implementierung einer Schnittstelle
*/

public class Human implements Mensch {
	private String name;

	public Human(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void gruessen() {
		System.out.println("Hallo! Ich bin "+getName()+"!");
	}

	public void essen() {
		System.out.println(getName()+" isst gerade Pizza.");
	}

	public void trinken() {
		System.out.println(getName()+" trinkt Bier.");
	}

	public void schlafen(int i) {
		System.out.println("Gute Nacht! "+getName()+" schläft für "+i+" Stunden.");
	}

	public void bewegen(int i) {
		System.out.println(getName()+" bewegt sich "+i+" Schritte nach vorne.");
	}

	public void tanzen() {
		System.out.println(getName()+" versucht es zu tanzen, aber kann leider nicht.");
	}
}