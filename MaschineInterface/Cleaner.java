/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Cleaner Klasse für Rainiers Tutorien, SoSe 2017
Implementierung der "Unterinterface" Reiniger
*/

public class Cleaner implements Reiniger {
	private int anzahlGereinigt;
	private String farbe = "weiß";
	private boolean istAn;
	private boolean istKaputt;
	private boolean istSchmutzig;
	
	public Cleaner() { }

	public Cleaner(String color) {
		farbe = color;
	}

	public int getAnzahl() {
		return anzahlGereinigt;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String color) {
		farbe = color;
	}

	public void anschalten() {
		istAn = true;
	}

	public void ausschalten() {
		istAn = false;
	}

	public boolean istKaputt() {
		return istKaputt;
	}

	public void reinigen() {
		System.out.println("Was soll ich denn reinigen?");
	}

	public void reinigen(Machine m) {
		if (m.istSchmutzig()) {
			m.setSchmutzig(false);
			anzahlGereinigt++;
			System.out.println(m.getName()+" ist jetzt sauber!");
		} else {
			System.out.println(m.getName()+" ist aber doch sauber.");
		}
	}

	public void reinigen(Cleaner c) {
		if (c.istSchmutzig()) {
			c.setSchmutzig(false);
			anzahlGereinigt++;
			System.out.println("Dein Reiniger ist jetzt sauber!");
		} else {
			System.out.println("Dein Reiniger ist aber doch sauber.");
		}
	}

	public void reinigen(Auto a) {
		if (a.istSchmutzig()) {
			a.setSchmutzig(false);
			anzahlGereinigt++;
			System.out.println("Dein Auto ist jetzt sauber!");
		} else {
			System.out.println("Dein Auto ist aber doch sauber.");
		}
	}

	public void showInfo() {
		System.out.println("Über deinen Reiniger:");
		System.out.println("Farbe: "+getFarbe());
		System.out.println("Hat bis jetzt "+getAnzahl()+" gereinigt.");
	}

	public boolean istSchmutzig() {
		return istSchmutzig;
	}

	public void setSchmutzig(boolean b) {
		istSchmutzig = b;
	}
	
}