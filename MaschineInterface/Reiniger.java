/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Reiniger "Unterinterface" für Rainiers Tutorien, SoSe 2017
*/

public interface Reiniger extends Maschine {
	void reinigen();
}