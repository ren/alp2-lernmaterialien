/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

MaschinenFabrik Klasse für Rainiers Tutorien, SoSe 2017
Zum Testen der Klassen Machine, Cleaner, und Auto
*/

public class MaschinenFabrik {
	
	public static void main(String[] args) {

		Machine machine = new Machine("Meine Maschine",4.2);
		machine.showInfo();
		System.out.println("---");

		Cleaner cleaner = new Cleaner("grün");
		cleaner.showInfo();
		System.out.println("---");

		Auto car = new Auto("rot", 230);
		car.showInfo();
		System.out.println("---");

		/* damit wir die reinigen Funktion testen können */
		car.setSchmutzig(true);
		machine.setSchmutzig(true);

		cleaner.reinigen(machine);
		cleaner.reinigen(cleaner);
		cleaner.reinigen(car);
		System.out.println("---");

		cleaner.showInfo();

	}
}