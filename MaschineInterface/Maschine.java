/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Maschine Interface für Rainiers Tutorien, SoSe 2017
*/

public interface Maschine {
	void anschalten();
	void ausschalten();
	void showInfo();
	boolean istKaputt();
	boolean istSchmutzig();
}