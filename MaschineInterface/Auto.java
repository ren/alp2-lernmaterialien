/*
Autor: Rainier Raymond Robles
Datum: 25. Juni 2017

Auto Klasse für Rainiers Tutorien, SoSe 2017
alternative Implementierung der Schnittstelle Maschine
*/

public class Auto implements Maschine {
	private String farbe;
	private int topspeed;
	private boolean istAn;
	private boolean istKaputt;
	private boolean istSchmutzig;

	public Auto() {}

	public Auto(String color, int topspeed) {
		farbe = color;
		this.topspeed = topspeed;
	}

	public int getTopspeed() {
		return topspeed;
	}

	public void setTopspeed(int x) {
		topspeed = x;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String color) {
		farbe = color;
	}

	public void anschalten() {
		istAn = true;
		System.out.println("Vroom Vroom");
	}

	public void ausschalten() {
		istAn = false;
	}

	public void showInfo() {
		System.out.println("Vroom!");
		System.out.println("Farbe: "+getFarbe());
		System.out.println("Maximalgeschwindigkeit: "+getTopspeed()+"km/h");
	}

	public boolean istKaputt() {
		return istKaputt;
	}

	public boolean istSchmutzig() {
		return istSchmutzig;
	}

	public void setSchmutzig(boolean b) {
		istSchmutzig = b;
	}

}